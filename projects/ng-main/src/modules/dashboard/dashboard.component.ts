import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {SearchResultViewModel} from '../../view-models/search-result.view-model';
import {PatientViewModel} from '../../view-models/patient.view-model';
import {of, Subject, Subscription} from 'rxjs';
import {catchError, finalize, switchMap} from 'rxjs/operators';
import {ISpinnerService, SPINNER_SERVICE_PROVIDER, WINDOW} from '@cms-ui/core';
import {SpinnerIds} from '../../constants/spinner-ids';
import {PATIENT_SERVICE_PROVIDER} from '../../constants/injectors';
import {IPatientService} from '../../services/interfaces/patient-service.interface';
import {LoadPatientViewModel} from '../../view-models/load-patient.view-model';
import {ToastrService} from 'ngx-toastr';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dashboard',
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit, OnDestroy {

  //#region Properties

  public page = 1;

  public readonly itemsPerPage = 10;

  protected onlineListener: any;

  protected offlineListener: any;

  private _loadPatientsResult: SearchResultViewModel<PatientViewModel>;

  private _loadPatientsSubject: Subject<void>;

  private _subscriptions: Subscription;

  //#endregion

  //#region Accessors

  public get patients(): PatientViewModel[] {
    if (!this._loadPatientsResult || !this._loadPatientsResult.items) {
      this._loadPatientsResult = new SearchResultViewModel<PatientViewModel>();
      return this._loadPatientsResult.items;
    }

    return this._loadPatientsResult.items;
  }

  public get totalRecords(): number {
    if (!this._loadPatientsResult) {
      this._loadPatientsResult = new SearchResultViewModel<PatientViewModel>();
      return 0;
    }

    return this._loadPatientsResult.totalRecords;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(SPINNER_SERVICE_PROVIDER) protected readonly spinnerService: ISpinnerService,
                     @Inject(PATIENT_SERVICE_PROVIDER) protected patientService: IPatientService,
                     protected toastrService: ToastrService) {
    this._loadPatientsSubject = new Subject<void>();
    this._subscriptions = new Subscription();
  }

  //#endregion

  //#region Life cycle

  public ngOnInit(): void {

    let activatedToast = null;

    this.offlineListener = () => {
      activatedToast = this.toastrService.warning('You are offline. Stay tune....');
    };

    this.onlineListener = () => {
      this.patientService.deletePatientsCache();
      if (activatedToast) {
        this.toastrService.clear(activatedToast);
        activatedToast = null;
      }

      this.toastrService.success('You are back online!');
    };

    window.addEventListener('offline', this.offlineListener);
    window.addEventListener('online', this.onlineListener);

    const loadPatientsSubscription = this._loadPatientsSubject
      .pipe(
        switchMap(() => {
          const displayedSpinnerId = this.spinnerService.displaySpinner(SpinnerIds.layout);
          const condition = new LoadPatientViewModel();
          condition.page = this.page;
          condition.totalRecords = this.itemsPerPage;

          return this.patientService.loadPatientsAsync(condition)
            .pipe(
              catchError(() => {
                const loadPatientsResult = new SearchResultViewModel<PatientViewModel>();
                loadPatientsResult.items = [];
                loadPatientsResult.totalRecords = this.totalRecords;
                return of(loadPatientsResult);
              }),
              finalize(() => this.spinnerService.deleteSpinner(SpinnerIds.layout, displayedSpinnerId))
            );
        })
      )
      .subscribe(loadPatientsResult => this._loadPatientsResult = loadPatientsResult);
    this._subscriptions.add(loadPatientsSubscription);

    this._loadPatientsSubject.next();
  }

  public ngOnDestroy(): void {
    window.removeEventListener('online', this.onlineListener);
    window.removeEventListener('offline', this.offlineListener);
    this._subscriptions.unsubscribe();
  }

  //#endregion

  //#region Methods

  public handlePageChanged(page: number): void {
    this.page = page;
    this._loadPatientsSubject.next();
  }

  //#endregion
}
