import {APP_INITIALIZER, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AppRouteModule} from './app.route';
import {AppSettingService} from '../services/implementations/app-setting.service';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {HttpLoaderFactory} from '../factories/ngx-translate.factory';
import {HttpClient} from '@angular/common/http';
import {NgRxMessageBusModule} from 'ngrx-message-bus';
import {appConfigServiceFactory} from '../factories/app-setting.factory';
import {APP_SETTINGS_SERVICE_PROVIDER} from '../constants/injectors';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import {BannerModule, SmartNavigatorModule, SpinnerContainerModule, WINDOW, WINDOW_PROVIDERS, WindowRef} from '@cms-ui/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {windowFactory} from '@cms-ui/core/services/implementations/window.service';
import {ToastrModule} from 'ngx-toastr';

//#region Module declaration

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    AppRouteModule,

    // Message bus registration.
    NgRxMessageBusModule.forRoot(),
    SmartNavigatorModule.forRoot(),
    BannerModule.forRoot(),
    SpinnerContainerModule.forRoot(),
    ToastrModule.forRoot(),

    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
      ServiceWorkerModule.register('ngsw-worker.js', {
        enabled: environment.production,
        // Register the ServiceWorker as soon as the app is stable
        // or after 30 seconds (whichever comes first).
        registrationStrategy: 'registerWhenStable:30000'
      }),
      NgbModule
  ],
  providers: [
    {
      provide: APP_SETTINGS_SERVICE_PROVIDER,
      useClass: AppSettingService
    },
    {
      provide: APP_INITIALIZER,
      useFactory: appConfigServiceFactory,
      multi: true,
      deps: [APP_SETTINGS_SERVICE_PROVIDER]
    }
  ],
  bootstrap: [AppComponent]
})


export class AppModule {
}

//#endregion
