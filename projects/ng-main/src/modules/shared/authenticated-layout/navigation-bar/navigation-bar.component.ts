import {Component, Inject, Input, OnDestroy} from '@angular/core';
import {Router} from '@angular/router';
import {IAuthenticationService} from '../../../../services/interfaces/authentication-service.interface';
import {ProfileViewModel} from '../../../../view-models/profile.view-model';
import {MessageChannelConstant} from '../../../../constants/message-channel.constant';
import {MessageEventConstant} from '../../../../constants/message-event.constant';
import {AUTHENTICATION_SERVICE_PROVIDER, UI_SERVICE_INJECTION_TOKEN} from '../../../../constants/injectors';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';
import {
  ISmartNavigatorService,
  NavigateToScreenRequest,
  SMART_NAVIGATOR_PROVIDER,
  SMART_NAVIGATOR_SCREEN_CODE_RESOLVER
} from '@cms-ui/core';
import {mergeMap} from 'rxjs/operators';
import {ScreenCodes} from '../../../../constants/screen-codes';
import {Subscription} from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[navigation-bar]',
  templateUrl: 'navigation-bar.component.html'
})

export class NavigationBarComponent implements OnDestroy {

  //#region Properties

  // Account property.
  // tslint:disable-next-line:no-input-rename
  @Input('profile')
  public profile: ProfileViewModel;

  /*
  * Whether sidebar should be visible or not.
  * */
  public shouldSidebarVisible = false;

  protected readonly subscription: Subscription;

  //#endregion

  //#region Constructor

  // Initiate instance with IoC.
  public constructor(@Inject(AUTHENTICATION_SERVICE_PROVIDER) protected authenticationService: IAuthenticationService,
                     @Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService,
                     @Inject(SMART_NAVIGATOR_PROVIDER) protected smartNavigatorService: ISmartNavigatorService,
                     public router: Router) {
    this.subscription = new Subscription();
  }

  //#endregion

  //#region Life cycle

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  //#endregion

  //#region Methods

  // Sign the user out.
  public clickSignOut(): void {

    const logOutSubscription = this.authenticationService.signOutAsync()
      .pipe(
        mergeMap(() => {
          const navigationRequest = new NavigateToScreenRequest(ScreenCodes.login);
          return this.smartNavigatorService.navigateToScreenAsync(navigationRequest);
        })
      )
      .subscribe();
    this.subscription.add(logOutSubscription);
  }

  /*
  * Called when side-bar toggle button is clicked.
  * */
  public clickSideBarToggle(): void {

    // Change side-bar visibility to opposite state.
    this.shouldSidebarVisible = !this.shouldSidebarVisible;

    // TODO: Implement message bus.
    this.messageBusService
      .addMessage<boolean>(MessageChannelConstant.ui, MessageEventConstant.displaySidebar, this.shouldSidebarVisible);
  }

  //#endregion
}
