import {Component, HostBinding, Inject, OnDestroy, OnInit} from '@angular/core';
import {LoginViewModel} from '../../view-models/login.view-model';
import {IAuthenticationService} from '../../services/interfaces/authentication-service.interface';
import {LoginResultViewModel} from '../../view-models/login-result-view-model';
import {Router} from '@angular/router';
import {MessageChannelConstant} from '../../constants/message-channel.constant';
import {MessageEventConstant} from '../../constants/message-event.constant';
import {AUTHENTICATION_SERVICE_PROVIDER} from '../../constants/injectors';
import {INgRxMessageBusService, MESSAGE_BUS_SERVICE_PROVIDER} from 'ngrx-message-bus';
import {
  ISmartNavigatorService,
  ISpinnerService,
  NavigateToScreenRequest,
  SMART_NAVIGATOR_PROVIDER,
  SPINNER_SERVICE_PROVIDER
} from '@cms-ui/core';
import {SpinnerIds} from '../../constants/spinner-ids';
import {finalize, mergeMap} from 'rxjs/operators';
import {ScreenCodes} from '../../constants/screen-codes';
import {Subscription} from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'div[account-login]',
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit, OnDestroy {

  //#region Properties

  public model: LoginViewModel;

  @HostBinding('class')
  public containerClass = 'container';

  protected readonly subscription: Subscription;

  //#endregion

  //#region Constructor

  public constructor(@Inject(AUTHENTICATION_SERVICE_PROVIDER) protected authenticationService: IAuthenticationService,
                     @Inject(MESSAGE_BUS_SERVICE_PROVIDER) protected messageBusService: INgRxMessageBusService,
                     @Inject(SPINNER_SERVICE_PROVIDER) protected readonly spinnerService: ISpinnerService,
                     @Inject(SMART_NAVIGATOR_PROVIDER) protected readonly smartNavigatorService: ISmartNavigatorService,
                     public router: Router) {
    this.model = new LoginViewModel();
    this.subscription = new Subscription();

  }

  //#endregion

  //#region Methods

  /*
  * Called when component is initialized.
  * */
  public ngOnInit(): void {
    this.messageBusService
      .addMessage<string>(MessageChannelConstant.ui, MessageEventConstant.updatePageClass, 'bg-gradient-primary');
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  /*
  * Callback which is fired when login button is clicked.
  * */
  public clickLogin($event): void {

    // Prevent default behaviour.
    if ($event) {
      $event.preventDefault();
    }

    const displayedSpinnerRequestId = this.spinnerService.displaySpinner(SpinnerIds.layout);
    const authenticationSubscription = this.authenticationService.authenticateAsync(this.model)
      .pipe(
        mergeMap(() => {
          const navigationRequest = new NavigateToScreenRequest(ScreenCodes.dashboard);
          return this.smartNavigatorService.navigateToScreenAsync(navigationRequest);
        }),
        finalize(() => this.spinnerService.deleteSpinner(SpinnerIds.layout, displayedSpinnerRequestId))
      )
      .subscribe();
    this.subscription.add(authenticationSubscription);
  }


  //#endregion
}
