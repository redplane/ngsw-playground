import {IPatientService} from '../interfaces/patient-service.interface';
import {Inject, Injectable} from '@angular/core';
import {LoadPatientViewModel} from '../../view-models/load-patient.view-model';
import {from, Observable, of} from 'rxjs';
import {SearchResultViewModel} from '../../view-models/search-result.view-model';
import {PatientViewModel} from '../../view-models/patient.view-model';
import {ENDPOINT_RESOLVER} from '../../constants/injectors';
import {IEndPointResolver} from '../interfaces/endpoint-resolver.interface';
import {map, mergeMap, tap} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class PatientService implements IPatientService {

  //#region Constructor

  public constructor(@Inject(ENDPOINT_RESOLVER) protected endPointResolver: IEndPointResolver,
                     protected httpClient: HttpClient) {
  }

  //#endregion

  //#region Methods

  public loadPatientsAsync(model: LoadPatientViewModel): Observable<SearchResultViewModel<PatientViewModel>> {

    const key = `loadPatientsAsync-${model.page}-${model.totalRecords}`;

    if (navigator.onLine) {

      return this.endPointResolver.getBaseUrlAsync()
        .pipe(
          mergeMap(baseUrl => {
            const fullUrl = `${baseUrl}/api/patient/search`;
            return this.httpClient.post<SearchResultViewModel<PatientViewModel>>(fullUrl, model);
          }),
          tap(loadPatientsResult => localStorage.setItem(key, JSON.stringify(loadPatientsResult)))
        );
    } else {
      const value = localStorage.getItem(key);
      if (!value || !value.length) {
        const loadPatientsResult = new SearchResultViewModel<PatientViewModel>();
        loadPatientsResult.items = [];
        loadPatientsResult.totalRecords = 0;

        return of(loadPatientsResult);
      }

      return of(JSON.parse(value));
    }
  }

  public deletePatientsCache(): void {
    this.deleteStorageItem(key => key.indexOf('loadPatientsAsync') !== -1);
  }

  //#endregion

  //#region Internal methods

  protected deleteStorageItem(filter: (key: string) => boolean): void {
    let index = 0;
    while (index < localStorage.length) {
      const addedKey = localStorage.key(index);
      if (filter(addedKey)) {
        localStorage.removeItem(addedKey);
        continue;
      }

      index++;
    }
  }

  //#endregion
}
