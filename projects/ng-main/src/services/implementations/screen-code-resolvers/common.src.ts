import {DefaultScreenCodeResolver} from '@cms-ui/core';
import {Injectable} from '@angular/core';
import {ScreenCodes} from '../../../constants/screen-codes';

@Injectable()
export class CommonSrc extends DefaultScreenCodeResolver {

  //#region Constructor

  constructor() {
    const codeToUrl = {};
    codeToUrl[ScreenCodes.login] = '/login';
    codeToUrl[ScreenCodes.dashboard] = '/dashboard';
    super(codeToUrl);
  }

  //#endregion
}
