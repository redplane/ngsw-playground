import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '../../models/app-settings';
import {environment} from '../../environments/environment';
import {merge as lodashMerge} from 'lodash-es';
import {IAppSettingService} from '../interfaces/app-setting-service.interface';
import {forkJoin, Observable, of} from 'rxjs';
import {catchError, map} from 'rxjs/operators';

@Injectable()
export class AppSettingService implements IAppSettingService {

  //#region Properties

  private _loadedSettings: AppSettings;

  //#endregion

  //#region Constructors

  constructor(public httpClient: HttpClient) {

  }

  //#endregion

  //#region Application configuration

  // Load app configuration from json file.
  public loadSettingsAsync(force?: boolean): Observable<AppSettings> {

    if (!force && this._loadedSettings) {
      return of(this._loadedSettings);
    }

    const loadAppSettingObservables = [];
    if (environment.configurations && environment.configurations.length) {
      for (const configuration of environment.configurations) {
        const loadAppSettingPromise = this.httpClient.get(configuration)
          .pipe(
            catchError(error => {
              console.error(error);
              return of([]);
            })
          );
        loadAppSettingObservables.push(loadAppSettingPromise);
      }
    }

    if (loadAppSettingObservables.length) {
      return forkJoin(loadAppSettingObservables)
        .pipe(
          map(loadAppSettingResults => {
            const appSettings = new AppSettings();
            for (const loadAppSettingResult of loadAppSettingResults) {
              lodashMerge(appSettings, loadAppSettingResult);
            }

            return appSettings;
          })
        );
    }

    this._loadedSettings = new AppSettings();
    return of(this._loadedSettings);
  }

  //#endregion
}
