import {IEndPointResolver} from '../interfaces/endpoint-resolver.interface';
import {Inject, Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {APP_SETTINGS_SERVICE_PROVIDER} from '../../constants/injectors';
import {IAppSettingService} from '../interfaces/app-setting-service.interface';
import {map} from 'rxjs/operators';
import {AppSettings} from '../../models/app-settings';

@Injectable()
export class EndPointResolver implements IEndPointResolver {

  //#region Constructor

  public constructor(@Inject(APP_SETTINGS_SERVICE_PROVIDER) protected appSettingService: IAppSettingService) {
  }

  //#endregion

  //#region Methods

  public getBaseUrlAsync(): Observable<string> {
    return this.appSettingService
      .loadSettingsAsync()
      .pipe(
        map((appSetting: AppSettings) => appSetting.baseUrl)
      );
  }

  //#endregion

}
