import {Injectable, Injector} from '@angular/core';
import {LoginResultViewModel} from '../../view-models/login-result-view-model';
import {IAuthenticationService} from '../interfaces/authentication-service.interface';
import {LoginViewModel} from '../../view-models/login.view-model';
import {Observable, of, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map, mergeMap, tap} from 'rxjs/operators';
import {StorageKeys} from '../../constants/storage-keys';
import {IEndPointResolver} from '../interfaces/endpoint-resolver.interface';
import {ENDPOINT_RESOLVER} from '../../constants/injectors';

@Injectable()
export class AuthenticationService implements IAuthenticationService {

  //#region Properties

  protected readonly endPointResolver: IEndPointResolver;

  protected readonly httpClient: HttpClient;

  //#endregion

  //#region Constructor

  public constructor(injector: Injector) {
    this.endPointResolver = injector.get(ENDPOINT_RESOLVER);
    this.httpClient = injector.get(HttpClient);
  }

  //#endregion

  //#region Methods

  public authenticateAsync(model: LoginViewModel): Observable<LoginResultViewModel> {

    if (navigator.onLine) {
      return this.endPointResolver
        .getBaseUrlAsync()
        .pipe(
          mergeMap((baseUrl: string) => {
            const fullUrl = `${baseUrl}/api/authentication`;
            return this.httpClient.post<LoginResultViewModel>(fullUrl, {})
              .pipe(
                map((loginResult: LoginResultViewModel) => {
                  localStorage.setItem(StorageKeys.accessToken, loginResult.accessToken);
                  localStorage.setItem(StorageKeys.loginResult, JSON.stringify(loginResult));
                  return loginResult;
                })
              );
          })
        );
    }

    const szLoginResult = localStorage.getItem(StorageKeys.loginResult);
    return of(JSON.parse(szLoginResult));
  }

  public loadAccessTokenAsync(): Observable<string> {
    const accessToken = localStorage.getItem(StorageKeys.accessToken);
    if (!accessToken) {
      return throwError('NO_ACCESS_TOKEN_FOUND');
    }
    return of(accessToken);
  }

  public signOutAsync(): Observable<void> {
    localStorage.removeItem(StorageKeys.accessToken);
    return of(void (0));
  }

  //#endregion
}
