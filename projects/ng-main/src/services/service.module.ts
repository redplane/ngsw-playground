import {NgModule, ModuleWithProviders} from '@angular/core';
import {AccountService} from './implementations/account.service';
import {AuthenticationService} from './implementations/authentication.service';
import {UiService} from './implementations/ui.service';
import {
  AUTHENTICATION_SERVICE_PROVIDER, ENDPOINT_RESOLVER, PATIENT_SERVICE_PROVIDER,
  UI_SERVICE_INJECTION_TOKEN,
  USER_SERVICE_INJECTION_TOKEN
} from '../constants/injectors';
import {SMART_NAVIGATOR_SCREEN_CODE_RESOLVER} from '@cms-ui/core';
import {CommonSrc} from './implementations/screen-code-resolvers/common.src';
import {EndPointResolver} from './implementations/end-point.resolver';
import {PatientService} from './implementations/patient.service';

@NgModule({})

export class ServiceModule {

  //#region Methods

  static forRoot(): ModuleWithProviders<ServiceModule> {
    return {
      ngModule: ServiceModule,
      providers: [
        {provide: USER_SERVICE_INJECTION_TOKEN, useClass: AccountService},
        {provide: AUTHENTICATION_SERVICE_PROVIDER, useClass: AuthenticationService},
        {provide: UI_SERVICE_INJECTION_TOKEN, useClass: UiService},
        {
          provide: ENDPOINT_RESOLVER,
          useClass: EndPointResolver
        },
        {
          provide: PATIENT_SERVICE_PROVIDER,
          useClass: PatientService
        },
        {
          provide: SMART_NAVIGATOR_SCREEN_CODE_RESOLVER,
          useClass: CommonSrc,
          multi: true
        }
      ]
    };
  }

  //#endregion
}


