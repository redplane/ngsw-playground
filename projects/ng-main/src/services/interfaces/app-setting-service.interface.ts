import {Observable} from 'rxjs';
import {AppSettings} from '../../models/app-settings';

export interface IAppSettingService  {

  //#region Methods

  loadSettingsAsync(force?: boolean): Observable<AppSettings>;

  //#endregion

}
