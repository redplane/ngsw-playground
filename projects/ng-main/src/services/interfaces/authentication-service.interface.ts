import {LoginResultViewModel} from '../../view-models/login-result-view-model';
import {Observable} from 'rxjs';
import {LoginViewModel} from '../../view-models/login.view-model';

export interface IAuthenticationService {

  //#region Methods

  // Authenticate into the system.
  authenticateAsync(model:  LoginViewModel): Observable<LoginResultViewModel>;

  // Sign out of the system.
  signOutAsync(): Observable<void>;

  // Load access token asynchronously.
  loadAccessTokenAsync(): Observable<string>;

  //#endregion
}
