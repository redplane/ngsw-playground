import {Observable} from 'rxjs';

export interface IEndPointResolver {

  //#region Methods

  getBaseUrlAsync(): Observable<string>;

  //#endregion

}
