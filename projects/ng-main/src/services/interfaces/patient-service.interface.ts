import {LoadPatientViewModel} from '../../view-models/load-patient.view-model';
import {Observable} from 'rxjs';
import {SearchResultViewModel} from '../../view-models/search-result.view-model';
import {PatientViewModel} from '../../view-models/patient.view-model';

export interface IPatientService {

  //#region Methods

  loadPatientsAsync(model: LoadPatientViewModel)
    : Observable<SearchResultViewModel<PatientViewModel>>;

  // Delete patients cache.
  deletePatientsCache(): void;

  //#endregion

}
