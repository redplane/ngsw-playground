export const environment = {
  production: false,
  configurations: ['/assets/appsettings.json', '/assets/appsettings.staging.json']
};
