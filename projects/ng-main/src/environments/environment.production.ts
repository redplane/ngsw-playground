export const environment = {
  production: true,
  configurations: ['/assets/appsettings.json', '/assets/appsettings.production.json']
};
