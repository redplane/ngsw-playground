export class PatientViewModel {

  //#region Properties

  public id: string;

  public name: string;

  public age: number;

  //#endregion

}
