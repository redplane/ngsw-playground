export class SearchResultViewModel<T> {

  //#region Properties

  public items: T[];

  public totalRecords: number;

  //#endregion

}
