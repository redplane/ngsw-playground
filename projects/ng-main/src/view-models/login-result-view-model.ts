// Class which stores information of token which is for authorizing user into system.
export class LoginResultViewModel {

  //#region Properties

  public accessToken: string;


  //#endregion
}
