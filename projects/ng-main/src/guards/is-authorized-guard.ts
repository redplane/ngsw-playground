import {Inject, Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';
import {AUTHENTICATION_SERVICE_PROVIDER} from '../constants/injectors';
import {Observable, of} from 'rxjs';
import {ISmartNavigatorService, SMART_NAVIGATOR_PROVIDER} from '@cms-ui/core';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {ScreenCodes} from '../constants/screen-codes';

@Injectable()
export class IsAuthorizedGuard implements CanActivate {

  //#region Constructor
  /*
  * Initiate guard component with injectors.
  * */
  public constructor(
    @Inject(AUTHENTICATION_SERVICE_PROVIDER) protected readonly authenticationService: IAuthenticationService,
    @Inject(SMART_NAVIGATOR_PROVIDER) protected readonly smartNavigatorService: ISmartNavigatorService) {
  }

  //#endregion

  //#region Methods

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authenticationService.loadAccessTokenAsync()
      .pipe(
        map(_ => {
          return true;
        }),
        catchError(() => {
          const loginUrlTree = this.smartNavigatorService.buildUrlTree(ScreenCodes.login);
          return of(loginUrlTree);
        })
      );
  }

  //#endregion
}
