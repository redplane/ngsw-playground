import {InjectionToken} from '@angular/core';
import {IAccountService} from '../services/interfaces/account-service.interface';
import {IAuthenticationService} from '../services/interfaces/authentication-service.interface';
import {IUiService} from '../services/interfaces/ui-service.interface';
import {IAppSettingService} from '../services/interfaces/app-setting-service.interface';
import {IEndPointResolver} from '../services/interfaces/endpoint-resolver.interface';
import {IPatientService} from '../services/interfaces/patient-service.interface';

// App setting service injection token.
export const APP_SETTINGS_SERVICE_PROVIDER = new InjectionToken<IAppSettingService>('APP_SETTINGS_SERVICE_PROVIDER');

export const USER_SERVICE_INJECTION_TOKEN = new InjectionToken<IAccountService>('Injection token of user service');

// tslint:disable-next-line:max-line-length
export const AUTHENTICATION_SERVICE_PROVIDER = new InjectionToken<IAuthenticationService>('Injection token of authentication service');


export const UI_SERVICE_INJECTION_TOKEN = new InjectionToken<IUiService>('Injection token of ui service');
export const PATIENT_SERVICE_PROVIDER = new InjectionToken<IPatientService>('PATIENT_SERVICE_PROVIDER');
export const ENDPOINT_RESOLVER = new InjectionToken<IEndPointResolver>('ENDPOINT_RESOLVER');
