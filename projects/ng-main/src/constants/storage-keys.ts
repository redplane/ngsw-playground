export class StorageKeys {

  //#region Properties

  public static readonly accessToken = 'ngsw-playground.access-token';

  public static readonly loginResult = 'ngsw-playground.login-result';

  //#endregion

}
